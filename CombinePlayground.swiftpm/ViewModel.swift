//
//  ViewModel.swift
//  CombinePlayground
//
//  Created by Krasilnikov Anton on 12.02.2024.
//

import Foundation
import Combine

final class ViewModel: ObservableObject {
    
    @Published var counter = "0"
    
    private var cancellables = [AnyCancellable]()
    
    func start() {
        cancellables.removeAll()
        
        MyTimerPublisher(time: 5)
            .subscribe(MySubscriberImpl<Int, Never>(receiveCompletion: { completion in
                return
            }, receiveValue: { [weak self] value in
                DispatchQueue.main.async {
                    self?.counter = String(value)
                }
            }, limit: 5))
        
//        MyTimerPublisher(time: 5)
//            .subscribe(Subscribers.Sink(receiveCompletion: { completion in
//                return
//            }, receiveValue: { [weak self] value in
//                DispatchQueue.main.async {
//                    self?.counter = String(value)
//                }
//            }))
//        
//        MyTimerPublisher(time: 5)
//            .sink { completion in
//                return
//            } receiveValue: { [weak self] value in
//                DispatchQueue.main.async {
//                    self?.counter = String(value)
//                }
//            }.store(in: &cancellables)
    }
    
    func stop() {
        counter = "0"
        cancellables.removeAll()
    }
}
