import Foundation
import Combine
import PlaygroundSupport

//  Вопросы:
//  1) Что такое Publisher в Optional<Int>.Publisher или Array<Int>().publisher что это за свойство?
//  2) У Just есть subject.send(subscription: <#T##Subscription#>) что это?
//  3) У Just и Optional (скорее всего) тип ошибки Never, почему тогда подписчик допускает в switch ошибку

//  Важно: (Рассмотрим пример с Future)
//  Рассмотрим пример с Future
//  Подписка нужна, чтобы при выходе из скоупа, где вызывается подписчик (в данным случае скоуп функции) замыкание sink/assign обработало свой вызов.
//  Даже если мы не сохраним подписку - замыкание все ровно будет вызвано, вот только если паблишер имеет какую-то долгую асинхронную операцию (как например тут) то наш подписчик не успеет его получить.
//  В итоге функция завершится -> мы выйдем из скоупа -> замыкание подписчика тоже не будет вызвано

// MyError

enum MyError: Error {
    case someError
}

// MARK: - Optional

func optional() {
    //  Как Just, тоже не имеет ошибки, но если значение NIL, то паблишер не отправит его
    //  ЗАТО response придет
    
    var cancellabels = [AnyCancellable]()
    let value: Int? = 123
    let optional: Optional<Int>.Publisher = value.publisher
    
    optional
        .replaceEmpty(with: 12345)
    // если паблишер не вернет никакого значения, то оператор заменит паблишер на Just(Value) со значение 12345
    // если же изнчально значение будет прокинуто, то этот опертор будет проигнорирован
    
        .sink { response in
            switch response {
            case .finished:
                print("First finished")
            case .failure(let error): // хотя кейс с ошибкой есть
                print("Value not found")
            }
        } receiveValue: { value in
            print(value)
        }
}

//optional()

// MARK: - Just

/// public func map<T>(_ transform: (Output) -> T) -> Just<T> берет тип паблишера (Output), и преобразует его в другой тип(T), при этом сама функция вернет тот же паблишер, на котором она и вызвана

func just() {
    let justPublisher: Just<Int?> =  Just(nil) // В отличии от Optional вернет значение, даже если оно nil
    
    //    justPublisher
    //        .map { value in
    //            value
    //        }
    
    justPublisher
        .replaceNil(with: 1111) // заменяет nil, который прокидывает Just(nil), а так же разврнет опционал
        .sink { response in
            switch response {
            case .finished:
                print("First finished")
            case .failure(let error):
                print("First finished")
            }
        } receiveValue: { value in
            print("First: \(String(describing: value))")
        }
    
    justPublisher
        .sink { response in
            switch response {
            case .finished:
                print("Second finished")
            }
        } receiveValue: { value in
            print("\nSecond: \(String(describing: value))")
        }
}

//just()

// MARK: - Subject

func  passthroughSubject() {
    var cancellables = [AnyCancellable]()
    
    let subject = PassthroughSubject<Int, Error>()
    
    subject.send(1) // у PassthroughSubject нет подписчиков -> некому отправлять данные -> 1 не будет показана
    
    subject
        .sink { completion in
            switch completion {
                
            case .finished:
                print("Success1")
            case .failure(let error):
                print("Fail1: \(error)")
            }
        } receiveValue: { value in
            print("\nFirst subscriber value: \(value)")
        }
    
    
    subject.send(2) // первый подписчик получит эти значения, но второй уже НЕТ
    subject.send(3)
    
    subject
        .sink { completion in
            switch completion {
                
            case .finished:
                print("Success2")
            case .failure(let error):
                print("Fail2: \(error)")
            }
        } receiveValue: { value in
            print("\nSecond subscriber value: \(value)")
        }
    
    subject.send(4) // оба получат 4, порядок получения - случайный
    subject.send(completion: .failure(MyError.someError)) // Комплишен срабатывает один раз, после него никакие значение больше не приходят
    subject.send(5)
}

//passthroughSubject()

func currentSubject() {
    var cancellables = [AnyCancellable]()
    
    let subject = CurrentValueSubject<Int, Error>(1) // обязаны указать начальное значение, есть буфер, где хранится последнее отправленное им значение
    
    // ВАЖНО: первым значением, которое получит подписчик будет последнее отправленное значение еще ДО СВОЕЙ ПОДПИСКИ, это означает, что первым в замыкание придет значение, которое было проинициализировалось или же которое мы отправили
    
    // subject.send(123) затерли 1 -> подписчки получит первым делом 123
    
    subject
        .sink { completion in
            switch completion {
            case .finished:
                print("Success1")
            case .failure(let error):
                print("Fail1: \(error)")
            }
        } receiveValue: { value in
            print("First subscriber newValue: \(value)")
        }
    
    print("\nOldValue: \(subject.value)")
    subject.send(3)
    
    subject
        .sink { completion in
            switch completion {
            case .finished:
                print("Success2")
            case .failure(let error):
                print("Fail2: \(error)")
            }
        } receiveValue: { value in
            print("Second subscriber newValue: \(value)")
        }
    
    print("\nOldValue: \(subject.value)")
    subject.send(4)
}

//currentSubject()

// MARK: - Future
var futureCancellables = [AnyCancellable]()
func future() { // Аналог закмыкания?? Выполняется тут же при объявлении, не дожидаюся подписчика
    
    print(1)
    let future = Deferred {
        //  Deferred - тоже паблишер, который принимает замыкание init(createPublisher: @escaping () -> DeferredPublisher)
        //  Он заставляет Future не начинать свое выполнение, пока не него не подпишутся
        //  ОДНАКО каждый новый подписчик будет получать новый результат ведь при новой подписке Deferred заставляет замыкание выполняться заново
        
        Future<Int, MyError> { promise in // promise: (Result<Int, Error>) -> Void то есть это замыкание, которое может жить внутри Future
            print("future began")
            DispatchQueue.global().asyncAfter(deadline: .now() + 3) {
                //  ЭТОТ КОММЕНТАРИЙ АКТУАЛЕН ЕСЛИ НЕ ИСПОЛЬЗОВАТЬ Deferred
                //  даже не смотря на на что выполнение идет не на главной очереди,  print(2) успевает вывестись раньше подписчика, подписчик все ровно получает значение promise(.success(777)) и выводит его
                
                let number = Int.random(in: 1...10)
                promise(.success(number))
                print(2)
            }
        }
    }
    
    print(3)
    
    //  sleep(4) // подождем
    
    future
        .sink { completion in
            switch completion {
            case .finished:
                print("Success")
            case .failure(let error):
                print("Fail: \(error)")
            }
        } receiveValue: { value in
            print("Value: \(value)")
        }
    //   .store(in: &futureCancellables)
    //  print(4)
    
    // sleep(4) // подождем
    future
        .sink { completion in
            switch completion {
            case .finished:
                print("Success")
            case .failure(let error):
                print("Fail: \(error)")
            }
        } receiveValue: { value in
            print("Value: \(value)")
        }
    //  .store(in: &futureCancellables)
    print(5)
}

//future()

//print(6)

// MARK: - Result

func result() {
    let result: Result<Int, Error> = .success(2) //.failure(MyError.someError) // при ошибке завершится ошибкой и ничего не отправит
    
    // publisher - свойство внутри Result (и не только в нем)
    // var publisher: Result<Success, Failure>.Publisher { get }
    // Publisher - структуру которая имплементирует протокол Publisher
    // проще говоря делает из enum Result паблишер (и не только из него)
    
    result.publisher
        .replaceEmpty(with: 123345)
        .sink { completion in
            switch completion {
            case .finished:
                print("Success")
            case .failure(let error):
                print("Fail: \(error)")
            }
            
        } receiveValue: { value in
            print("Value: \(value)")
        }
}

//result()

// MARK: - Empty

func empty() {
    
    let empty = Empty<Int, Error>()
    
    empty // не имеет значения и не имеет assign
        .replaceEmpty(with: 123) // Empty не имеет никакого значения, он просто сообщает о завершении потока, replaceEmpty придаст паблишеру значение
        .sink { completion in
            switch completion {
            case .finished:
                print("Success")
            case .failure(let error):
                print("Fail: \(error)")
            }
            
        } receiveValue: { value in
            print("Value: \(value)")
        }
    
    empty // не имеет значения и не имеет assign
        .replaceEmpty(with: 123) // Empty не имеет никакого значения, он просто сообщает о завершении потока, replaceEmpty придаст паблишеру значение
        .sink { completion in
            switch completion {
            case .finished:
                print("Success")
            case .failure(let error):
                print("Fail: \(error)")
            }
            
        } receiveValue: { value in
            print("Value: \(value)")
        }
}

//empty()

// MARK: - URLSession

func urlSession() {
    guard let url = URL(string: "https://developer.apple.com/swift/images/swift-og.png") else {
        return
    }
    let urlSession = URLSession.shared.dataTaskPublisher(for: url)
    
    
    // func dataTaskPublisher(for url: URL) -> URLSession.DataTaskPublisher
    
        .map { data, responce in // map в себе принимет именно Output, поэтому тут мы не можем достучаться до ошибки
            data
        }
}


// MARK: - Costume Publisher
// MyTimerPublisher

struct MyTimerPublisher: Publisher {
    
    typealias Output = Int
    typealias Failure = Never
    
    private(set) var time: UInt
    
    func receive<S>(subscriber: S) where S : Subscriber, Never == S.Failure, Int == S.Input {
        guard let subscriber = subscriber as? Subscribers.MySubscriberImpl<Output, Failure> else {
            return
        }
        
        let subscription = MyTimerSubscription(subscriber: subscriber)
        subscriber.receive(subscription: subscription)
    }
}

extension MyTimerPublisher {
    func mySink(receiveCompletion: @escaping ((Subscribers.Completion<Self.Failure>) -> Void), receiveValue: @escaping ((Self.Output) -> Void)) -> AnyCancellable {
        
        let sink = Subscribers.MySubscriberImpl<Self.Output, Self.Failure>(receiveCompletion: receiveCompletion, receiveValue: receiveValue, limit: Int(time))
        
        //subscribe(sink)
        return AnyCancellable(MyTimerSubscription(subscriber: sink))
    }
}

// MyTimerSubscription

class MyTimerSubscription<SubscriberType: MySubscriber>: Subscription where SubscriberType.Input == Int, SubscriberType.Failure == Never  {
    
    private var subscriber: SubscriberType?
    private var myTimer: Timer?
    private var counter: Int?
    private var isCancel: Bool = false
    
    init(subscriber: SubscriberType) {
        self.subscriber = subscriber

        self.subscriber?.stop = { [weak self] in
            self?.cancel()
        }
    }
    
    func request(_ demand: Subscribers.Demand) {
        counter = demand.max
        startTimer()
    }
    
    func cancel() {
        subscriber = nil
        isCancel = true
        print("Подписка")
    }
    
    private func startTimer() {
        DispatchQueue.global(qos: .background).async {
            self.myTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.start), userInfo: nil, repeats: true)
            
            guard let myTimer = self.myTimer else {
                return
            }
            let runLoop = RunLoop.current
            runLoop.add(myTimer, forMode: .common)
            runLoop.run()
        }
    }
    
    @objc func start() {
        guard var counter = counter else {
            return
        }
        
        guard isCancel == false, counter >= 0 else {
            subscriber?.receive(completion: .finished)
            myTimer?.invalidate()
            return
        }
        _ = self.subscriber?.receive(counter)
        counter -= 1
        self.counter = counter
    }
    
    deinit {
        print("MyTimerSubscription deinited")
    }
}

// MySubscriber

protocol MySubscriber: Subscriber {
    
    var stop: (() -> ())? { get set }
    
}
extension Subscribers {
    class MySubscriberImpl<Input, Failure> : MySubscriber, Cancellable {
        
        typealias Input = Int
        typealias Failure = Never
        
        var receiveValue: ((Int) -> Void)?
        var receiveCompletion: ((Subscribers.Completion<Never>) -> Void)?
        var stop: (() -> ())?
        
        private var limit: Int
        
        init(receiveCompletion: @escaping ((Subscribers.Completion<Never>) -> Void), receiveValue: @escaping ((Int) -> Void), limit: Int) {
            self.receiveCompletion = receiveCompletion
            self.receiveValue = receiveValue
            self.limit = limit
        }
        
        func receive(subscription: Subscription) {
            subscription.request(.max(limit))
        }
        
        func receive(_ input: Int) -> Subscribers.Demand {
            receiveValue?(input)
            
            return .none
        }
        
        func receive(completion: Subscribers.Completion<Never>) {
            receiveCompletion?(completion)
            stop?()
        }
        
        func cancel() {
            stop?()
            print("Подписчик")
        }
        
        deinit {
            print("MySubscriber deinited")
        }
    }
}

var cancellables = [AnyCancellable]()
func myTimer() {
    let timer = MyTimerPublisher(time: 2)
    
   let timer1 = timer
        .mySink { completion in
            if completion == .finished {
                print("Completed")
            }
        } receiveValue: { value in
            print(value)
        }
    
    timer1
        .store(in: &cancellables)
    
//    timer1
//        .cancel()
}

//DispatchQueue.global(qos: .utility).async {
    myTimer()
//}


//print("Конец")
cancellables.removeAll()

PlaygroundPage.current.needsIndefiniteExecution = true

