//
//  Publisher.swift
//  CombinePlayground
//
//  Created by Krasilnikov Anton on 12.02.2024.
//

import Combine
import Foundation

// MyTimerPublisher

struct MyTimerPublisher: Publisher {
    
    typealias Output = Int
    typealias Failure = Never
    
    private(set) var time: UInt
    
    func receive<S>(subscriber: S) where S : Subscriber, Never == S.Failure, Int == S.Input {
        
        let subscription = MyTimerSubscription(subscriber: subscriber, counter: Int(time))
        subscriber.receive(subscription: subscription)
    }
}

// MyTimerSubscription

class MyTimerSubscription<SubscriberType: Subscriber>: Subscription where SubscriberType.Input == Int, SubscriberType.Failure == Never  {
    
    private var subscriber: SubscriberType?
    private var myTimer: Timer?
    private var counter: Int
    private var isCancel: Bool = false
    
    init(subscriber: SubscriberType, counter: Int) {
        self.subscriber = subscriber
        self.counter = counter
    }
    
    func request(_ demand: Subscribers.Demand) {
        startTimer()
    }
    
    func cancel() {
        subscriber = nil
        isCancel = true
        print("Подписка")
    }
    
    private func startTimer() {
        DispatchQueue.global(qos: .background).async {
            self.myTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.start), userInfo: nil, repeats: true)
            
            guard let myTimer = self.myTimer else {
                return
            }
            
            let runLoop = RunLoop.current
            runLoop.add(myTimer, forMode: .common)
            runLoop.run()
        }
    }
    
    @objc func start() {
        guard isCancel == false, counter >= 0 else {
            subscriber?.receive(completion: .finished)
            myTimer?.invalidate()
            return
        }
        _ = self.subscriber?.receive(counter)
        counter -= 1
    }
    
    deinit {
        print("MyTimerSubscription deinited")
    }
}

// MySubscriber

class MySubscriberImpl<Input, Failure> : Subscriber, Cancellable {
    
    typealias Input = Int
    typealias Failure = Never
    
    var receiveValue: ((Int) -> Void)?
    var receiveCompletion: ((Subscribers.Completion<Never>) -> Void)?
    
    private var limit: Int
    private var subscription: Subscription?
    
    init(receiveCompletion: @escaping ((Subscribers.Completion<Never>) -> Void), receiveValue: @escaping ((Int) -> Void), limit: Int) {
        self.receiveCompletion = receiveCompletion
        self.receiveValue = receiveValue
        self.limit = limit
    }
    
    func receive(subscription: Subscription) {
        self.subscription = subscription
        self.subscription?.request(.max(limit))
    }
    
    func receive(_ input: Int) -> Subscribers.Demand {
        receiveValue?(input)
        return .none
    }
    
    func receive(completion: Subscribers.Completion<Never>) {
        receiveCompletion?(completion)
        cancel()
    }
    
    deinit {
        print("MySubscriber deinited")
    }
    
    func cancel() {
        subscription?.cancel()
        subscription = nil
    }
}
