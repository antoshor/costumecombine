import Foundation

enum KeychainError: Error {
    case error(OSStatus)
}

class KeychainService {
    
    // save
    func save(password: String, server: String, account: String) throws {
        let keychainItem = [
            kSecValueData: password.data(using: .utf8)!, // наш пароль, который мы хотим сохранить
            
            // атрибуты нужны для поиска нашего пароля среди:
            kSecAttrAccount: account, // аккаунта, типа ник "Anton2000"
            kSecAttrServer: server, // сервера: "gmail.com"
            kSecClass: kSecClassInternetPassword, // тип нашего пароля
          
            kSecReturnData: true, // вернем наш пароль
            kSecReturnAttributes: true // хотим ли мы вернуть данные
        ] as CFDictionary
        
        var ref: AnyObject?

        let status = SecItemAdd(keychainItem, &ref)
        
        guard status == errSecSuccess else { // проверем статус и выкидываем ошибку
            throw KeychainError.error(status)
        }
        
        let result = ref as! NSDictionary
        print("Operation finished with status: \(status)")
       
        let passwordData = result[kSecValueData] as! Data
        let passwordString = String(data: passwordData, encoding: .utf8)
        print("Password: \(String(describing: passwordString))")
        
        print("Username: \(result[kSecAttrAccount] ?? "")")
    }
    
    // get
    func get(server: String) throws { // вернем наши данные для сервера
        let query = [
            kSecClass: kSecClassInternetPassword,
            kSecAttrServer: server, // укажем, что ищем пароль для СЕРВЕРА
            kSecReturnAttributes: true,
            kSecReturnData: true,
            kSecMatchLimit: 5 // чтобы веруть несколько значений, тк таких паролей может быть несколько
        ] as CFDictionary
        
        var result: AnyObject?
        let status = SecItemCopyMatching(query, &result)
        
        guard status == errSecSuccess else { // проверем статус и выкидываем ошибку
            throw KeychainError.error(status)
        }
        
        print("Operation finished with status: \(status)")
        let array = result as! [NSDictionary] // если мы указали kSecMatchLimit, то и вернем мы массив СЛОВАРЕЙ.
        
        array.forEach { dic in
            let username = dic[kSecAttrAccount] ?? ""
            let passwordData = dic[kSecValueData] as! Data
            let password = String(data: passwordData, encoding: .utf8)!
            print("\nUsername: \(username)")
            print("Password: \(password)")
        }
    }
    
    // update
    func update(server: String, account: String, newPassword: String) throws {
        let query = [
            kSecClass: kSecClassInternetPassword,
            kSecAttrServer: server, // уточняем пароль для чего именно мы хотим обновить
            kSecAttrAccount: account
        ] as CFDictionary
        
        let updateFields = [kSecValueData: newPassword.data(using: .utf8)!] as CFDictionary // новый пароль
        
        let status = SecItemUpdate(query, updateFields)
        
        
        guard status == errSecSuccess else { // проверем статус и выкидываем ошибку
            throw KeychainError.error(status)
        }
        print("Operation finished with status: \(status)")
    }
    
    // delete
    func delete(server: String, account: String) throws {
        let query = [
          kSecClass: kSecClassInternetPassword,
          kSecAttrServer: server,
          kSecAttrAccount: account
        ] as CFDictionary

        let status = SecItemDelete(query) // принимает только CFDictionary с тем дынными по которым мы хотим удалить наш пароль
        
        guard status == errSecSuccess else { // проверем статус и выкидываем ошибку
            throw KeychainError.error(status)
        }
    }
}

let keyChain = KeychainService()

//do {
//    try keyChain.delete(server: "anton.com", account: "Anton")
//    print("Success")
//} catch let error {
//    print(error)
//}

//do {
//    try keyChain.save(password: "12345", server: "anton.com", account: "Anton")
//
//} catch let error {
//    print(error)
//}

//do {
//    try keyChain.update(server: "anton.com", account: "Anton", newPassword: "anton123")
//} catch let error {
//    print(error)
//}

//do {
//    try keyChain.get(server: "anton.com")
//} catch let error {
//    print(error)
//}
//

