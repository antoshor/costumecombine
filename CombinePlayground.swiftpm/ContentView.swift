import SwiftUI
import Combine

struct ContentView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack {
            
            Text(viewModel.counter)
            
            Button {
                DispatchQueue.global().async {
                    viewModel.start()
                }
                
            } label: {
                ZStack {
                    Capsule()
                    Text("Старт")
                        .foregroundColor(.white)
                }
            }
            .frame(height: 100)
            
            Button {
                viewModel.stop()
            } label: {
                ZStack {
                    Capsule()
                        .foregroundColor(.green)
                    Text("Стоп")
                        .foregroundColor(.white)
                }
            }
            .frame(height: 100)
        }
        .padding()
    }
}
